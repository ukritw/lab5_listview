package tu.ac.th.lab5customerlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddNewActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addnew);
		Button saveBt = (Button) findViewById(R.id.save);
		saveBt.setOnClickListener(this);
		
		Intent i = this.getIntent();
//		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
//		EditText etRate = (EditText) findViewById(R.id.etRate);
//		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText name = (EditText) findViewById(R.id.name);
		EditText tel = (EditText) findViewById(R.id.tel);
		EditText packag = (EditText) findViewById(R.id.packag);
		try {
			Intent data = new Intent();
			data.putExtra("name", name.getText().toString());
			data.putExtra("tel", tel.getText().toString());
			data.putExtra("package", packag.getText().toString());
			this.setResult(RESULT_OK, data);
			this.finish();
		} catch (NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(),
					"Invalid input", Toast.LENGTH_SHORT);
			t.show();
		}
	}


}
