package tu.ac.th.lab5customerlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SimpleAdapter;

public class MainActivity extends ListActivity {
	List<Map<String, String>> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		data = this.getSampleData();
		SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.items,
				new String[] { "name", "phone", "package" }, new int[] {
						R.id.tvName, R.id.tvPhone, R.id.tvPackage });
		this.setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent dataReturn) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			String name = dataReturn.getStringExtra("name");
			String tel = dataReturn.getStringExtra("tel");
			String packag = dataReturn.getStringExtra("package");
			Map<String, String> m = new HashMap<String, String>();
			m.put("name", name);
			m.put("phone", tel);
			m.put("package", packag);
			data.add(m);
			SimpleAdapter adapter = (SimpleAdapter) this.getListAdapter();
			adapter.notifyDataSetChanged();
		}
	}
	
	List<Map<String, String>> getSampleData() {
		List<Map<String, String>> l = new ArrayList<Map<String, String>>();
		Map<String, String> m = new HashMap<String, String>();
		m.put("name", "Bill Gate");
		m.put("phone", "1-000-000-0000");
		m.put("package", "Unlimited minutes/data");
		l.add(m);
		m = new HashMap<String, String>();
		m.put("name", "Steve Jobs");
		m.put("phone", "1-111-111-1111");
		m.put("package", "Unlimited minutes/data");
		l.add(m);
		return l;
	}

}
